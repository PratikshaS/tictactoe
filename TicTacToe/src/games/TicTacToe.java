package games;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import java.awt.Color;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.SwingConstants;

public class TicTacToe {

	private JFrame frame;
	private String StartGame ="X";
	private int xCount = 0;
	private int oCount = 0;
	private int flag1 = 0;
	private int flag2 = 0;
	private int flag3 = 0;
	private int flag4 = 0;
	private int flag5 = 0;
	private int flag6 = 0;
	private int flag7 = 0;
	private int flag8 = 0;
	private int flag9 = 0;
	private JPanel panel_1;
	private JPanel panel_2;
	private JPanel panel_3;
	private JPanel panel_4;
	private JPanel panel_5;
	private JPanel panel_6;
	private JPanel panel_7;
	private JPanel panel_8;
	private JPanel panel_9;
	private JPanel panel_10;
	private JPanel panel_11;
	private JPanel panel_12;
	private JPanel panel_13;
	private JPanel panel_14;
	private JPanel panel_15;
	private JButton btn1;
	private JButton btn2;
	private JButton btn3;
	private JButton btn4;
	private JButton btn5;
	private JButton btn6;
	private JButton btn7;
	private JButton btn8;
	private JButton btn9;
	private JLabel lblPlayerX;
	private JButton btnReset;
	private JButton btnExit;
	private JTextField XScore;
	private JTextField OScore;
	private JLabel lblPlayerO;
	private JButton btnNew;
	private JPanel panel_16;
	private JPanel panel_17;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TicTacToe window = new TicTacToe();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public TicTacToe() {
		initialize();
		GameScore();
	}
	private void reset() {
		flag1 = 0;
		flag2 = 0;
		flag3 = 0;
		flag4 = 0;
		flag5 = 0;
		flag6 = 0;
		flag7 = 0;
		flag8 = 0;
		flag9 = 0;
		btn1.setText(null);
		btn2.setText(null);
		btn3.setText(null);
		btn4.setText(null);
		btn5.setText(null);
		btn6.setText(null);
		btn7.setText(null);
		btn8.setText(null);
		btn9.setText(null);
		StartGame = "X";
	}
	private void GameScore() {
		XScore.setText(String.valueOf(xCount));
		OScore.setText(String.valueOf(oCount));
	}
	private void ChoosePlayer() {
		if(StartGame.equalsIgnoreCase("X")) {
			StartGame = "O";
		}
		else {
			StartGame = "X";
		}
	}
	private void winningPlayer(){
		String b1 =  btn1.getText();
		String b2 =  btn2.getText();
		String b3 =  btn3.getText();
		String b4 =  btn4.getText();
		String b5 =  btn5.getText();
		String b6 =  btn6.getText();
		String b7 =  btn7.getText();
		String b8 =  btn8.getText();
		String b9 =  btn9.getText();
	///////// Player X winning Combination////////////
		if(b1 == ("X") && b2 == ("X") && b3 == ("X")) {
			JOptionPane.showMessageDialog(frame, "player X Wins", "Tic Tac Toe",JOptionPane.INFORMATION_MESSAGE);
			xCount++;
			GameScore();
			reset();
		}
		if(b4 == ("X") && b5 == ("X") && b6 == ("X")) {
			JOptionPane.showMessageDialog(frame, "player X Wins", "Tic Tac Toe",JOptionPane.INFORMATION_MESSAGE);
			xCount++;
			GameScore();
			reset();
		}
		if(b7 == ("X") && b8 == ("X") && b9 == ("X")) {
			JOptionPane.showMessageDialog(frame, "player X Wins", "Tic Tac Toe",JOptionPane.INFORMATION_MESSAGE);
			xCount++;
			GameScore();
			reset();
		}
		if(b1 == ("X") && b4 == ("X") && b7 == ("X")) {
			JOptionPane.showMessageDialog(frame, "player X Wins", "Tic Tac Toe",JOptionPane.INFORMATION_MESSAGE);
			xCount++;
			GameScore();
			reset();
		}
		if(b2 == ("X") && b5 == ("X") && b8 == ("X")) {
			JOptionPane.showMessageDialog(frame, "player X Wins", "Tic Tac Toe",JOptionPane.INFORMATION_MESSAGE);
			xCount++;
			GameScore();
			reset();
		}
		if(b3 == ("X") && b6 == ("X") && b9 == ("X")) {
			JOptionPane.showMessageDialog(frame, "player X Wins", "Tic Tac Toe",JOptionPane.INFORMATION_MESSAGE);
			xCount++;
			GameScore();
			reset();
		}
		if(b1 == ("X") && b5 == ("X") && b9 == ("X")) {
			JOptionPane.showMessageDialog(frame, "player X Wins", "Tic Tac Toe",JOptionPane.INFORMATION_MESSAGE);
			xCount++;
			GameScore();
			reset();
		}
		if(b3 == ("X") && b5 == ("X") && b7 == ("X")) {
			JOptionPane.showMessageDialog(frame, "player X Wins", "Tic Tac Toe",JOptionPane.INFORMATION_MESSAGE);
			xCount++;
			GameScore();
			reset();
		}
		//////////// Winning Player 2//////
		if(b1 == ("O") && b2 == ("O") && b3 == ("O")) {
			JOptionPane.showMessageDialog(frame, "player O Wins", "Tic Tac Toe",JOptionPane.INFORMATION_MESSAGE);
			oCount++;
			GameScore();
			reset();
		}
		if(b4 == ("O") && b5 == ("O") && b6 == ("O")) {
			JOptionPane.showMessageDialog(frame, "player O Wins", "Tic Tac Toe",JOptionPane.INFORMATION_MESSAGE);
			oCount++;
			GameScore();
			reset();
		}
		if(b7 == ("O") && b8 == ("O") && b9 == ("O")) {
			JOptionPane.showMessageDialog(frame, "player O Wins", "Tic Tac Toe",JOptionPane.INFORMATION_MESSAGE);
			oCount++;
			GameScore();
			reset();
		}
		if(b1 == ("O") && b4 == ("O") && b7 == ("O")) {
			JOptionPane.showMessageDialog(frame, "player O Wins", "Tic Tac Toe",JOptionPane.INFORMATION_MESSAGE);
			oCount++;
			GameScore();
			reset();
		}
		if(b2 == ("O") && b5 == ("O") && b8 == ("O")) {
			JOptionPane.showMessageDialog(frame, "player O Wins", "Tic Tac Toe",JOptionPane.INFORMATION_MESSAGE);
			oCount++;
			GameScore();
			reset();
		}
		if(b3 == ("O") && b6 == ("O") && b9 == ("O")) {
			JOptionPane.showMessageDialog(frame, "player O Wins", "Tic Tac Toe",JOptionPane.INFORMATION_MESSAGE);
			oCount++;
			GameScore();
			reset();
		}
		if(b1 == ("O") && b5 == ("O") && b9 == ("O")) {
			JOptionPane.showMessageDialog(frame, "player O Wins", "Tic Tac Toe",JOptionPane.INFORMATION_MESSAGE);
			oCount++;
			GameScore();
			reset();
		}
		if(b3 == ("O") && b5 == ("O") && b7 == ("O")) {
			JOptionPane.showMessageDialog(frame, "player O Wins", "Tic Tac Toe",JOptionPane.INFORMATION_MESSAGE);
			oCount++;
			GameScore();
			reset();
		}
	}
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1200, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(Color.BLACK));
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(3, 5, 2, 2));
		
		panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		panel.add(panel_1);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		btn1 = new JButton("");
		btn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(flag1 == 0)
				{
				btn1.setText(String.valueOf(StartGame));
					if(StartGame.equalsIgnoreCase("X")) {
						btn1.setForeground(Color.RED);
					}
					else {
						btn1.setForeground(Color.BLUE);
					}
					ChoosePlayer();
					flag1 = 1;
					winningPlayer();
				}
				
			}
		});
		btn1.setFont(new Font("Lucida Bright", Font.PLAIN, 93));
		panel_1.add(btn1, BorderLayout.CENTER);
		
		panel_2 = new JPanel();
		panel_2.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		panel.add(panel_2);
		panel_2.setLayout(new BorderLayout(0, 0));
		
		btn2 = new JButton("");
		btn2.setFont(new Font("Lucida Bright", Font.BOLD, 93));
		btn2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(flag2 == 0)
				{
				btn2.setText(String.valueOf(StartGame));
				if(StartGame.equalsIgnoreCase("X")) {
					btn2.setForeground(Color.RED);
				}
				else {
					btn2.setForeground(Color.BLUE);
				}
				ChoosePlayer();
				flag2 = 1;
				winningPlayer();
			}
			
			}
		});
		panel_2.add(btn2, BorderLayout.CENTER);
		
		panel_3 = new JPanel();
		panel_3.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		panel.add(panel_3);
		panel_3.setLayout(new BorderLayout(0, 0));
		
		btn3 = new JButton("");
		btn3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(flag3 == 0)
				{
				btn3.setText(String.valueOf(StartGame));
				if(StartGame.equalsIgnoreCase("X")) {
					btn3.setForeground(Color.RED);
				}
				else {
					btn3.setForeground(Color.BLUE);
				}
				ChoosePlayer();
				flag3 = 1;
				winningPlayer();
			}
				
			}
		});
		btn3.setFont(new Font("Lucida Bright", Font.BOLD, 93));
		panel_3.add(btn3, BorderLayout.CENTER);
		
		panel_4 = new JPanel();
		panel_4.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		panel.add(panel_4);
		panel_4.setLayout(new BorderLayout(0, 0));
		
		lblPlayerX = new JLabel("Player X:");
		lblPlayerX.setHorizontalAlignment(SwingConstants.CENTER);
		lblPlayerX.setFont(new Font("Lucida Bright", Font.BOLD, 48));
		lblPlayerX.setEnabled(true);
		panel_4.add(lblPlayerX, BorderLayout.CENTER);
		
		panel_5 = new JPanel();
		panel_5.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		panel.add(panel_5);
		panel_5.setLayout(new BorderLayout(0, 0));
		
		XScore = new JTextField();
		XScore.setHorizontalAlignment(SwingConstants.CENTER);
		XScore.setFont(new Font("Lucida Bright", Font.BOLD, 93));
		panel_5.add(XScore, BorderLayout.CENTER);
		XScore.setColumns(10);
		
		panel_6 = new JPanel();
		panel_6.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		panel.add(panel_6);
		panel_6.setLayout(new BorderLayout(0, 0));
		
		btn4 = new JButton("");
		btn4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(flag4 == 0)
				{
				btn4.setText(String.valueOf(StartGame));
				if(StartGame.equalsIgnoreCase("X")) {
					btn4.setForeground(Color.RED);
				}
				else {
					btn4.setForeground(Color.BLUE);
				}
				ChoosePlayer();
				flag4 = 1;
				winningPlayer();
			}
				
			}
		});
		btn4.setFont(new Font("Lucida Bright", Font.BOLD, 93));
		panel_6.add(btn4, BorderLayout.CENTER);
		
		panel_7 = new JPanel();
		panel_7.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		panel.add(panel_7);
		panel_7.setLayout(new BorderLayout(0, 0));
		
		btn5 = new JButton("");
		btn5.setFont(new Font("Lucida Bright", Font.BOLD, 93));
		btn5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(flag5 == 0)
				{
				btn5.setText(String.valueOf(StartGame));
				if(StartGame.equalsIgnoreCase("X")) {
					btn5.setForeground(Color.RED);
				}
				else {
					btn5.setForeground(Color.BLUE);
				}
				ChoosePlayer();
				flag5 = 1;
				winningPlayer();
			}
				
			}
		});
		panel_7.add(btn5, BorderLayout.CENTER);
		
		panel_8 = new JPanel();
		panel_8.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		panel.add(panel_8);
		panel_8.setLayout(new BorderLayout(0, 0));
		
		btn6 = new JButton("");
		btn6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(flag6 == 0)
				{
				btn6.setText(String.valueOf(StartGame));
				if(StartGame.equalsIgnoreCase("X")) {
					btn6.setForeground(Color.RED);
				}
				else {
					btn6.setForeground(Color.BLUE);
				}
				ChoosePlayer();
				flag6 = 1;
				winningPlayer();
			}	
			}
		});
		btn6.setFont(new Font("Lucida Bright", Font.BOLD, 93));
		panel_8.add(btn6, BorderLayout.CENTER);
		
		panel_9 = new JPanel();
		panel_9.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		panel.add(panel_9);
		panel_9.setLayout(new BorderLayout(0, 0));
		
		lblPlayerO = new JLabel("Player O:");
		lblPlayerO.setHorizontalAlignment(SwingConstants.CENTER);
		lblPlayerO.setFont(new Font("Lucida Bright", Font.BOLD, 48));
		panel_9.add(lblPlayerO, BorderLayout.CENTER);
		
		panel_10 = new JPanel();
		panel_10.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		panel.add(panel_10);
		panel_10.setLayout(new BorderLayout(0, 0));
		
		OScore = new JTextField();
		OScore.setHorizontalAlignment(SwingConstants.CENTER);
		OScore.setFont(new Font("Lucida Bright", Font.BOLD, 93));
		OScore.setText("0");
		panel_10.add(OScore, BorderLayout.CENTER);
		OScore.setColumns(10);
		
		panel_11 = new JPanel();
		panel_11.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		panel.add(panel_11);
		panel_11.setLayout(new BorderLayout(0, 0));
		
		btn7 = new JButton("");
		btn7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(flag7== 0)
				{
				btn7.setText(String.valueOf(StartGame));
				if(StartGame.equalsIgnoreCase("X")) {
					btn7.setForeground(Color.RED);
				}
				else {
					btn7.setForeground(Color.BLUE);
				}
				ChoosePlayer();
				flag7 = 1;
				winningPlayer();				}
			}
		});
		btn7.setFont(new Font("Lucida Bright", Font.BOLD, 93));
		panel_11.add(btn7, BorderLayout.CENTER);
		
		panel_12 = new JPanel();
		panel_12.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		panel.add(panel_12);
		panel_12.setLayout(new BorderLayout(0, 0));
		
		btn8 = new JButton("");
		btn8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(flag8 == 0)
				{
				btn8.setText(String.valueOf(StartGame));
				if(StartGame.equalsIgnoreCase("X")) {
					btn8.setForeground(Color.RED);
				}
				else {
					btn8.setForeground(Color.BLUE);
				}
				ChoosePlayer();
				flag8 = 1;
				winningPlayer();
				}
			}
		});
		btn8.setFont(new Font("Lucida Bright", Font.BOLD, 93));
		panel_12.add(btn8, BorderLayout.CENTER);
		
		panel_13 = new JPanel();
		panel_13.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		panel.add(panel_13);
		panel_13.setLayout(new BorderLayout(0, 0));
		
		btn9 = new JButton("");
		btn9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(flag9 == 0)
				{
				btn9.setText(String.valueOf(StartGame));
				if(StartGame.equalsIgnoreCase("X")) {
					btn9.setForeground(Color.RED);
				}
				else {
					btn9.setForeground(Color.BLUE);
				}
				ChoosePlayer();
				flag9 = 1;
				winningPlayer();
			}
			}
		});
		btn9.setFont(new Font("Lucida Bright", Font.BOLD, 93));
		panel_13.add(btn9, BorderLayout.CENTER);
		
		panel_14 = new JPanel();
		panel_14.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		panel.add(panel_14);
		panel_14.setLayout(new GridLayout(0, 1, 0, 0));
		
		panel_16 = new JPanel();
		panel_16.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_14.add(panel_16);
		panel_16.setLayout(new BorderLayout(0, 0));
		
		btnNew = new JButton("New");
		panel_16.add(btnNew);
		btnNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				reset();
				xCount = 0;
				oCount = 0;
				GameScore();
			}
		});
		btnNew.setFont(new Font("Lucida Bright", Font.BOLD, 48));
		
		panel_17 = new JPanel();
		panel_17.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_14.add(panel_17);
		panel_17.setLayout(new BorderLayout(0, 0));
		
		btnReset = new JButton("Reset");
		panel_17.add(btnReset);
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				reset();				
//				btn1.setBackground(Color.LIGHT_GRAY);
			}
		});
		btnReset.setFont(new Font("Lucida Bright", Font.BOLD, 48));
		
		panel_15 = new JPanel();
		panel_15.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		panel.add(panel_15);
		panel_15.setLayout(new BorderLayout(0, 0));
		
		btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame = new JFrame("Exit");
				if(JOptionPane.showConfirmDialog(frame, "Confirm if you want to exit","Tic Tac Toe",
						JOptionPane.YES_NO_OPTION) == JOptionPane.YES_NO_OPTION) {
					System.exit(0);
				}
				
			}
		});
		btnExit.setFont(new Font("Lucida Bright", Font.BOLD, 48));
		panel_15.add(btnExit, BorderLayout.CENTER);
	}

}
